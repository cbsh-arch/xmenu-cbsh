#!/bin/sh

xmenu <<EOF | sh &
Applications
	IMG:./icons/web.png		Web Browser		brave
	IMG:./icons/gimp.png	Image editor	gimp

Terminals
	Terminal (alacritty)	alacritty
	Terminal (kitty)	kitty
	Terminal (st)		st

Power
	Logout			$HOME/dmenu-scripts/scripts/dm-logout
	Shutdown		poweroff
	Reboot			reboot
EOF
