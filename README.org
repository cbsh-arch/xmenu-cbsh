#+TITLE: Xmenu-cbsh
#+AUTHOR: Cameron Miller
#+DESCRIPTION: A Customised build of Xmenu by https://github.com/phillbush for the CBSH Desktop

* About Xmenu
XMenu is a menu utility for X. XMenu receives a menu specification in stdin, shows a menu for the user to select one of the options, and outputs the option selected to stdout. XMenu can be controlled both via mouse and via keyboard.

* Xmenu's Featureset
- XMenu reads something in and prints something out, the UNIX way.
- Submenus (some menu entries can spawn another menu).
- Separators (menu entries can be separated by a line).
- Icons (menu entries can be preceded by an icon image).
- X resources support (you don't need to recompile xmenu for configuring it).
- Multi-head (xmenu supports multiple monitors by using Xinerama).
- Type-to-select (you can select an item by typing part of its name).

* Installing Xmenu
** Via CBSH or the cbsh-arch-repo
If you have the cbsh-arch-repo enabled (either manually or via the CBSH script) then you can install Xmenu via ~sudo pacman -S xmenu-cbsh~

** Installing Manually
First, edit ~./config.mk~ to match your local setup.

In order to build XMenu you need the ~Imlib2~, ~Xlib~, ~Xinerama~ and ~Xft~ header files. The default configuration for XMenu is specified in the file ~config.h~, you can edit it, but most configuration can be changed at runtime via X resources. Enter the following command to build XMenu. This command creates the binary file ~./xmenu~.

#+begin_src
make
#+end_src

By default, XMenu is installed into the ~/usr/local~ prefix. Enter the following command to install XMenu (if necessary as root). This command installs the binary file ~./xmenu~ into the ~${PREFIX}/bin/~ directory, and the manual file ~./xmenu.1~ into ~${MANPREFIX}/man1/~ directory.

#+begin_src
make install
#+end_src
